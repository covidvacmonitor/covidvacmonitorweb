FROM node:14-slim as build
ARG BUILD_ENV
WORKDIR /usr/src/app
ENV PATH=${PATH}:./node_modules/.bin
ENV NODE_PATH=/srv/node_modules
ADD package.json .
ADD package-lock.json .
RUN npm ci
ADD . .
RUN if [ "$BUILD_ENV" = "docker" ]; \
    then npm run build:ssr:docker; \
    else npm run build:ssr; \
    fi

FROM node:14-alpine as server
WORKDIR /usr/src/app
COPY --from=build /usr/src/app/dist ./dist
CMD [ "node", "dist/CovidVacMonitorWeb/server/main.js"]
