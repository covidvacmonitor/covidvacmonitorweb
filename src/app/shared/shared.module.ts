import { ModalNotificacaoComponent } from './components/modal-notificacao/modal-notificacao.component';
import { LoadingComponent } from './components/loading/loading.component';
import { CheckBoxComponent } from './components/checkbox/checkbox.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CovidVacMonitorService } from './services/covidvcmonitorapi.service';

const componentes = [CheckBoxComponent, LoadingComponent, ModalNotificacaoComponent];

@NgModule({
  imports: [CommonModule],
  exports: [...componentes, CommonModule],
  declarations: [...componentes],
  providers: [CovidVacMonitorService],
})
export class SharedModule { }
