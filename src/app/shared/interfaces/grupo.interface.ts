export enum Dose {
  primeira = 1,
  segunda = 3
};

export enum Tipo {
  IDOSOACIMA60 = 0,
  IDOSO60A65 = 1,
  IDOSOACIMA70 = 2,
  IDOSOACIMA65 = 3,
  IDOSOACIMA75 = 4,
  IDOSOACIMA80 = 5,
  TRABALHADORSAUDE = 6,
  NAOIDENTIFICADO = 7
};

export interface Grupo {
  disponibilidade: boolean;
  texto: string;
  value: number;
  tipo: Tipo;
  dose: Dose;
}
