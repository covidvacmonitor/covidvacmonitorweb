import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule, USE_EMULATOR as USE_FIRESTORE_EMULATOR } from '@angular/fire/firestore';

import { NgModule } from '@angular/core';
import { environment } from 'src/environments/environment';

@NgModule({
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule.enablePersistence()
  ],
  exports: [
    AngularFireModule,
    AngularFirestoreModule
  ],
  providers: [
    { provide: USE_FIRESTORE_EMULATOR, useValue: environment.useFirestoreEmulator ? ['localhost', 8200] : undefined}
  ]
})
export class FireModule { }
