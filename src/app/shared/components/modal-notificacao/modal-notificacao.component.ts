import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-notificacao',
  templateUrl: 'modal-notificacao.component.html',
  styleUrls: ['modal-notificacao.component.scss']
})

export class ModalNotificacaoComponent implements OnInit {
  @Input() exibir = false;

  constructor() { }

  ngOnInit(): void {

   }
}
