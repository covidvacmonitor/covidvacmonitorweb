import { LocalGrupo } from './../../../shared/interfaces/localGrupo.interface';
import { Retorno } from './../../../shared/interfaces/retorno.interface';
import { Component, OnInit } from '@angular/core';
import { CovidVacMonitorService } from 'src/app/shared/services/covidvcmonitorapi.service';
import { finalize, tap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'tile-info',
  templateUrl: 'tile-info.component.html'
})

export class TileInfoComponent implements OnInit {

  paginaAtual = 1;
  qtdPaginas = 1;
  qtdItens = 6;
  dadosPaginados: LocalGrupo[] = [];

  retorno!: Retorno<LocalGrupo[]>;

  isLoading = false;
  isOnline = false;

  constructor(
    private cvmonService: CovidVacMonitorService
  ) { }

  ngOnInit() {
    this.getDisponiveis();
  }

  getDisponiveis() {
    this.isLoading = true;
    this.cvmonService.disponiveis()
    .pipe(
      tap(retorno => {
        this.qtdPaginas = retorno.data.length > 0 ? Math.ceil(retorno.data.length / this.qtdItens) : 1;
        this.dadosPaginados = retorno.data.slice(0, this.qtdItens);
      })
    )
    .subscribe({
      next: retorno => {
        this.retorno = retorno;
        this.paginar(1);
        this.isLoading = false;
      },
      error: (error: HttpErrorResponse) => {
        console.log(error);
        this.isLoading = false;
        this.isOnline = navigator.onLine;
      }
    })
  }

  paginar(pagina: number) {
    if (pagina < 1 || pagina > this.qtdPaginas ) { return }
    this.paginaAtual = pagina;
    this.dadosPaginados = this.retorno.data.slice((pagina - 1) * this.qtdItens, pagina * this.qtdItens);
  }

}
