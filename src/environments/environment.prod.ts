export const environment = {
  production: true,
  SERVER_URL: 'https://api.vacinacovid.dodev.dev',
  VAPID_PUBLIC: 'BBEbQDqlftL5bOKFOSogXqZIm1iGjXCNq6kVjDJx930-5V6HrK3xFrN98J5ZgbPB8QnuGKANMAxv6wQOPF7e578',
  firebaseConfig: {
    apiKey: "AIzaSyCOh0ezC6f4_0k0hknhdw4ytF6Bhcaqnvg",
    authDomain: "monitor-vacina-covid.firebaseapp.com",
    projectId: "monitor-vacina-covid",
    storageBucket: "monitor-vacina-covid.appspot.com",
    messagingSenderId: "515467938764",
    appId: "1:515467938764:web:e7923ec4d6f41fadcc2b69"
  },
  useFirestoreEmulator: false
};
