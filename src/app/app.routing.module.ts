import { LayoutComponent } from './core/pages/layout/layout.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const ROUTES: Routes = [
  { path: '', component: LayoutComponent,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) }
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES, {
    initialNavigation: 'enabled'
})
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
