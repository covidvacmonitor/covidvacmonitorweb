export interface Retorno<T> {
  sucesso: boolean,
  userErro?: {code?: string, mensagem?: string},
  logErro?: any,
  data: T
}
